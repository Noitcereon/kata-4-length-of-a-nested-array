let kataNumber = 4;
let kataName = `Kata ${kataNumber}: Length of a Nested Array`;
document.title = kataName;
document.getElementById("frontPageH1").innerText = kataName;

// Gets the length of the array, counting any nested arrays items as well.
function getLength(anArray) {
    let totalArrayLength = 0;
    for (let item of anArray) {
        if (Array.isArray(item)) {
            totalArrayLength += getLength(item);
            continue;
        }
        totalArrayLength++;
    }
    return totalArrayLength;
}

// Examples of desired functionality
const outcome1 = getLength([1, [2, 3]])                     // ➞ 3
const outcome2 = getLength([1, [2, [3, 4]]])                // ➞ 4
const outcome3 = getLength([1, [2, [3, [4, [5, 6]]]]])      // ➞ 6
const outcome4 = getLength([1, [2], 1, [2], 1])             // ➞ 5
const outcome5 = getLength([1, [2], 1, [2, [5, 3], 2], 1])  // ➞ 8

console.log(outcome1);
console.log(outcome2);
console.log(outcome3);
console.log(outcome4);
console.log(outcome5);


// Notes
// An empty array should return 0.